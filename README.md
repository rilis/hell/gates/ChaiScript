## ChaiScript proxy repository for hell

This is proxy repository for [ChaiScript librarary](https://github.com/ChaiScript/ChaiScript), which allow you to build and install it using [hell dependency manager](https://gitlab.com/rilis/hell/hell).

* If you have problem with installation of ChaiScript using hell, have improvement idea, or want to request support for other versions of ChaiScript, then please [create issue here](https://gitlab.com/rilis/rilis/issues).
* If you found bug in ChaiScript itself please create issue on [ChaiScript issue tracker](https://github.com/ChaiScript/ChaiScript/issues), because here we don't do any kind of ChaiScript development.